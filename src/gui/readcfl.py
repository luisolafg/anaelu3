#!/usr/bin/python

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

class CflData(object):

    '''functionless data class'''
    def __init__(self):

        #self.window.title_edit.setText("Platin") #title

        #self.window.a_edit.setText("3.91") #a
        #self.window.b_edit.setText("3.91") b
        #self.window.c_edit.setText("3.91")  c

        #self.window.a2_edit.setText("90")
        #self.window.b2_edit.setText("90")
        #self.window.c2_edit.setText("90")

        #self.window.space_group_edit.setText("225") #spgr
        #self.window.average_crystal_edit.setText("1.05")
        #self.window.IPF_steps_edit.setText("1440")
        #self.window.h_edit.setText("1")
        #self.window.k_edit.setText("1")
        #self.window.l_edit.setText("1")
        #self.window.IPF_FWHM_edit.setText("6")
        #self.window.inverse_pole_edit.setText("0.5")

        #self.window.z_edit.setText("Pt")
        #self.window.label_edit.setText("Pt")
        #self.window.xpos_edit.setText("0")
        #self.window.ypos_edit.setText("0")
        #self.window.zpos_edit.setText("0")
        #self.window.dwf_edit.setText("0.5")

        self.title = None
        self.new_inputtitle = None

        self.spgr           = None
        self.a              = None
        self.b              = None
        self.c              = None
        self.alpha          = None
        self.beta           = None
        self.gamma          = None

        self.lg_size        = None
        self.new_avg_strain = None
        self.h              = None
        self.k              = None
        self.l              = None

        self.hklwh          = None
        self.azm_ipf        = None
        self.ipf_res        = None
        self.size_lg        = None
        self.mask_min       = None


def read_cfl_file(my_file_path = "My_Cfl.cfl"):

    print("my_file_path =", my_file_path)

    myfile = open(my_file_path, "r")

    all_lines = myfile.readlines()
    myfile.close()

    lst_dat = []

    data = CflData()



    for lin_char in all_lines:
        commented = False
        spgr_found = False

        # TODO make sure ther is no symbols in Herman Maguin notation
        # been excluded here

        for delim in ',;=':
            lin_char = lin_char.replace(delim, ' ')

        lst_litle_blocks = lin_char.split()
        for pos, litle_block in enumerate(lst_litle_blocks):

            if( litle_block == "!" or litle_block == "#" ):
                commented = True

            if( litle_block.upper() == "SPGR" ):
                print("found SPGR")
                simlbl = ""
                print("<<<"),
                for litl_litl_block in lst_litle_blocks[pos+1:]:
                    print ("[", litl_litl_block, "]"),
                    simlbl += " " + litl_litl_block
                print (">>>")
                data.spgr = simlbl
                spgr_found = True

            if( not commented and litle_block != "," and not spgr_found):
                lst_dat.append(litle_block)


    for pos, lab_flag in enumerate(lst_dat):
        if( lab_flag.upper() == "TITLE" ):
            data.title = lst_dat[pos + 1]

        elif( lab_flag.upper() == "CELL" ):
            data.a = lst_dat[pos + 1]
            data.b = lst_dat[pos + 2]
            data.c = lst_dat[pos + 3]
            data.alpha = lst_dat[pos + 4]
            data.beta  = lst_dat[pos + 5]
            data.gamma = lst_dat[pos + 6]

        elif( lab_flag.upper() == "SIZE_LG" ):
            data.size_lg = lst_dat[pos + 1]

        elif( lab_flag.upper() == "IPF_RES" ):
            data.ipf_res = lst_dat[pos + 1]

        elif( lab_flag.upper() == "HKL_PREF" ):
            data.h = lst_dat[pos + 1]
            data.k = lst_dat[pos + 2]
            data.l = lst_dat[pos + 3]

        elif( lab_flag.upper() == "HKL_PREF_WH" ):
            data.hklwh = lst_dat[pos + 1]

        elif( lab_flag.upper() == "AZM_IPF" ):
            data.azm_ipf = lst_dat[pos + 1]

        elif( lab_flag.upper() == "MASK_MIN" ):
            data.mask_min = lst_dat[pos + 1]


    return data



def write_cfl(dto, do_close = True):

    '''creating the .cfl file from the data read in the gui'''


    myfile = open("cfl_out.cfl", "w")

    if( dto.new_inputtitle != None ):
        wrstring = "Title  " + dto.new_inputtitle + "\n"
        myfile.write(wrstring);


    if( dto.a != None ):
        wrstring = "!      a      b      c     alpha    beta   gamma \n"
        myfile.write(wrstring);
        wrstring = "Cell   " + dto.a + "  " + dto.b + "  " + \
                    dto.c + "   " + dto.alpha + "   " + \
                    dto.beta + "  " + dto.gamma + "\n"
        myfile.write(wrstring);


    if( dto.spgr != None ):
        wrstring = "!     Space Group" + "\n" + "Spgr  " + dto.spgr + "\n"
        myfile.write(wrstring);


    if( dto.lg_size != None ):
        wrstring = "SIZE_LG  " + dto.lg_size + "\n"
        myfile.write(wrstring);

    if( dto.ipf_res != None ):
        wrstring = "IPF_RES  " + dto.ipf_res + "\n"
        myfile.write(wrstring);

    if( dto.h != None and dto.k != None and dto.l != None ):
        wrstring =  "HKL_PREF  " + dto.h + " " + dto.k + " " + dto.l + "\n"
        myfile.write(wrstring);

    if( dto.hkl_wh != None ):
        wrstring = "HKL_PREF_WH  " + dto.hkl_wh + "\n"
        myfile.write(wrstring);

    if( dto.azm_ipf != None ):
        wrstring = "AZM_IPF  " + dto.azm_ipf + "\n"
        myfile.write(wrstring);

    if( dto.mask_min != None ):
        wrstring = "MASK_MIN  " + dto.mask_min + "\n"
        myfile.write(wrstring);

    if(do_close):
        myfile.close()
    else:
        wrstring = "!                  x         y        z      B" + "\n"
        myfile.write(wrstring);

        return myfile


if( __name__ == "__main__" ):
    cfl_file = read_cfl_file(my_file_path = "My_Cfl.cfl")
