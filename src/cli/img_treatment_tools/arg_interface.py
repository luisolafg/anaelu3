# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

def print_params(par2print):
    for p_pair in par2print:
        print( p_pair[0], " = ", p_pair[1], "\n")


def get_par(par_def, lst_in):
    '''
    Reused function for handling parameters given via C.L.I.
    '''

    if(len(lst_in) == 0):
        print( "\n empty call")
        print( "\n default params:")
        print(_params(par_def))
        return None #TODO find out how to interrupt execution entirely


    try:

        par_out = []
        for par in par_def:
            par_out.append([par[0], par[1]])

        lst_split_tst = []
        for par in lst_in:
            lst_split_tst.append(par.split("="))

        lng_n1 = len(lst_split_tst[0])
        for lng_tst in lst_split_tst:
            if(len(lng_tst) != lng_n1):
                lng_n1 = None
                break


        if(lng_n1 == None):
            print( "Error 01")

        elif(lng_n1 == 1):
            for pos, par in enumerate(lst_in):
                par_out[pos][1] = lst_in[pos]

        elif(lng_n1 == 2):
            for par in lst_in:
                lf_rg_lst=par.split("=")
                print( "lf_rg_lst=", lf_rg_lst)
                for pos, iter_par in enumerate(par_def):
                    if(iter_par[0] == lf_rg_lst[0]):
                        par_out[pos][1] = lf_rg_lst[1]

        else:
            print( "Error 02")

        #TODO there is no way to check if the only argument is not the first one


    except:
        print( "par_def:", par_def)
        print( "lst_in:", lst_in)

    print( "\nentered params:")
    print_params(par_out)

    return par_out

if(__name__ == "__main__"):
    par_def_01 =(("cfl_in", "my_cfl.cfl"),
                 ("dat_in", "my_dat.dat"),
                 ("dummy_var", 5.2),
                 ("file_out", "my_edf.edf"))

    lst_in_01 = ["file1.cfl","file2.dat"]
    lst_in_02 = ["cfl_in=file1.cfl","dat_in=file2.dat", "file_out=tst"]
    lst_in_03 = ["file1.cfl","file2.dat", "tst"]
    lst_in_04 = ["file1.cfl","x=file2.dat", "tst"]
    lst_in_05 = ["x=file1.cfl=y","z=file2.dat=w", "1=tst=2"]
    lst_in_06 = ["cfl_in=file1.cfl","dat_in=file2.dat", "file_out=tst", "dummy_var=3.4"]

    par_lst_out = get_par(par_def_01, lst_in_06)

    print( "par_lst_out =", par_lst_out)
    print( "par_def_01(overwrite) =", par_def_01)
