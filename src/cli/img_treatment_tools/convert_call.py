#import tools
#from matplotlib import pyplot as plt
import numpy as np
import fabio
import sys
from arg_interface import get_par

if( __name__ == "__main__" ):

    par_def =(("img_in", "dummy.mccd"),
              ("img_out", "dummy.edf"))

    #print "\n sys.argv =", sys.argv, "\n"
    in_par = get_par(par_def, sys.argv[1:])
    #print "in_par =", in_par

    path_to_img = in_par[0][1]
    img_file_out =  in_par[1][1]
    #print "Len(in_par) =", len(in_par)

    print("img_in =", path_to_img)
    img_in = fabio.open(path_to_img).data.astype(np.float64)

    if(len(in_par) == 2 ):
        res_img = fabio.edfimage.edfimage()
        res_img.data = img_in

        print("img_file_out =", img_file_out)
        res_img.write(str(img_file_out))

    else:
        print("something went wrong here")


