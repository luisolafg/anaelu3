# Anaelu3

Version 3 of the texture XRD 2D project Anaelu

We aim to build with Gfortran, CrysFml, Python3 and PySide2 a version of Anaelu,
capable to do some basic automatic refinement of 2D pattern of axially textured
crystalline samples.
